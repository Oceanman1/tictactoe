#pragma once

#include <conio.h>
#include <iostream>
#include <string>

class TicTacToe
{

private:

	//fields
	char m_board[3][3] = { 
		{' ', ' ', ' '},
		{' ', ' ', ' '}, 
		{' ', ' ', ' '}
	};
	int m_numTurns = 0;
	char m_playerTurn = 'X';
	char m_winner = ' ';

public:

	//constructor
	TicTacToe() { }

	//mutators


	//other methods
	virtual void DisplayBoard()
	{
	
		for (int y = 0; y < 3; y++)
		{
			for (int x = 0; x < 3; x++)
			{
				std::cout << m_board[y][x] << " ";
			}
			std::cout << "\n";
		}
	}

	virtual bool IsOver()
	{
		if (isblank(m_winner))
		{
			return false;
		}
		else if (m_winner == 'X' || m_winner == 'O' || m_winner == 'T')
		{
			return true;
		}
	}

	virtual char GetPlayerTurn()
	{
		if (m_playerTurn == 'X')
		{
			return 'X';
		}
		else if (m_playerTurn == 'O')
		{
			return 'O';
		}
	}

	virtual bool IsValidMove(int position)
	{
		if (position == 1)
		{
			if(m_board[0][0] == 'X' || m_board[0][0] == 'O')
			{
				return false;
			}
			else if (m_board[0][0] == ' ')
			{
				return true;
			}
		}
		else if (position == 2)
		{
			if (m_board[0][1] == 'X' || m_board[0][1] == 'O')
			{
				return false;
			}
			else if (m_board[0][1] == ' ')
			{
				return true;
			}
		}
		else if (position == 3)
		{
			if (m_board[0][2] == 'X' || m_board[0][2] == 'O')
			{
				return false;
			}
			else if (m_board[0][2] == ' ')
			{
				return true;
			}
		}
		else if (position == 4)
		{
			if (m_board[1][0] == 'X' || m_board[1][0] == 'O')
			{
				return false;
			}
			else if (m_board[1][0] == ' ')
			{
				return true;
			}
		}
		else if (position == 5)
		{
			if (m_board[1][1] == 'X' || m_board[1][1] == 'O')
			{
				return false;
			}
			else if (m_board[1][1] == ' ')
			{
				return true;
			}
		}
		else if (position == 6)
		{
			if (m_board[1][2] == 'X' || m_board[1][2] == 'O')
			{
				return false;
			}
			else if (m_board[1][2] == ' ')
			{
				return true;
			}
		}
		else if (position == 7)
		{
			if (m_board[2][0] == 'X' || m_board[2][0] == 'O')
			{
				return false;
			}
			else if (m_board[2][0] == ' ')
			{
				return true;
			}
		}
		else if (position == 8)
		{
			if (m_board[2][1] == 'X' || m_board[2][1] == 'O')
			{
				return false;
			}
			else if (m_board[2][1] == ' ')
			{
				return true;
			}
		}
		else if (position == 9)
		{
			if (m_board[2][2] == 'X' || m_board[2][2] == 'O')
			{
				return false;
			}
			else if (m_board[2][2] == ' ')
			{
				return true;
			}
		}
		
	}

	virtual void Move(int position)
	{
		
		if (m_playerTurn == 'X')
		{
			if (position == 1)
			{
				m_board[0][0] = 'X';
			}
			else if (position == 2)
			{
				m_board[0][1] = 'X';
			}
			else if (position == 3)
			{
				m_board[0][2] = 'X';
			}
			else if (position == 4)
			{
				m_board[1][0] = 'X';
			}
			else if (position == 5)
			{
				m_board[1][1] = 'X';
			}
			else if (position == 6)
			{
				m_board[1][2] = 'X';
			}
			else if (position == 7)
			{
				m_board[2][0] = 'X';
			}
			else if (position == 8)
			{
				m_board[2][1] = 'X';
			}
			else if (position == 9)
			{
				m_board[2][2] = 'X';
			}

			if ((m_board[0][0] == 'X' && m_board[0][1] == 'X' && m_board[0][2] == 'X') || (m_board[1][0] == 'X' && m_board[1][1] == 'X' && m_board[1][2] == 'X')
				|| (m_board[2][0] == 'X' && m_board[2][1] == 'X' && m_board[2][2] == 'X') || (m_board[0][0] == 'X' && m_board[1][0] == 'X' && m_board[2][0] == 'X')
				|| (m_board[0][1] == 'X' && m_board[1][1] == 'X' && m_board[2][1] == 'X') || (m_board[0][2] == 'X' && m_board[1][2] == 'X' && m_board[2][2] == 'X')
				|| (m_board[0][0] == 'X' && m_board[1][1] == 'X' && m_board[2][2] == 'X') || (m_board[0][2] == 'X' && m_board[1][1] == 'X' && m_board[2][0] == 'X'))
			{
				m_winner = 'X';
			}

			m_numTurns++;

			if (m_numTurns == 9)
			{
				m_winner = 'T';
			}

			m_playerTurn = 'O';
		}
		else if (m_playerTurn == 'O')
		{ 
			if (position == 1)
			{
				m_board[0][0] = 'O';
			}
			else if (position == 2)
			{
				m_board[0][1] = 'O';
			}
			else if (position == 3)
			{
				m_board[0][2] = 'O';
			}
			else if (position == 4)
			{
				m_board[1][0] = 'O';
			}
			else if (position == 5)
			{
				m_board[1][1] = 'O';
			}
			else if (position == 6)
			{
				m_board[1][2] = 'O';
			}
			else if (position == 7)
			{
				m_board[2][0] = 'O';
			}
			else if (position == 8)
			{
				m_board[2][1] = 'O';
			}
			else if (position == 9)
			{
				m_board[2][2] = 'O';
			}

			if ((m_board[0][0] == 'O' && m_board[0][1] == 'O' && m_board[0][2] == 'O') || (m_board[1][0] == 'O' && m_board[1][1] == 'O' && m_board[1][2] == 'O')
				|| (m_board[2][0] == 'O' && m_board[2][1] == 'O' && m_board[2][2] == 'O') || (m_board[0][0] == 'O' && m_board[1][0] == 'O' && m_board[2][0] == 'O')
				|| (m_board[0][1] == 'O' && m_board[1][1] == 'O' && m_board[2][1] == 'O') || (m_board[0][2] == 'O' && m_board[1][2] == 'O' && m_board[2][2] == 'O')
				|| (m_board[0][0] == 'O' && m_board[1][1] == 'O' && m_board[2][2] == 'O') || (m_board[0][2] == 'O' && m_board[1][1] == 'O' && m_board[2][0] == 'O'))
			{
				m_winner = 'O';
			}
			m_numTurns++;

			m_playerTurn = 'X';
		}
	}

	virtual void DisplayResult()
	{
		if (m_winner == 'X')
		{
			std::cout << "X is the winner!\n";
		}
		else if (m_winner == 'O')
		{
			std::cout << "O is the winner!\n";
		}
		else if (m_winner == 'T')
		{
			std::cout << "Tie game...\n";
		}
	}
};